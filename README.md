# Logo Crawler

Project built using Scrapy. 

Dependencies are listed in requirements.txt. To install the dependencies, run 
```sh
pip install -r requirements.txt
```

To execute the code, from the root directory of the project, run 
```sh
scrapy crawl logo_scraper
```

The images will be saved in the root folder under ```.\logos```, with seperate subdirectories for each of the websites. 

All the data crawled has been moved to ```.\Generated Data``` to prevent overwritting if the code is re-executed after cloning.

A comprehensive JSON file and a CSV file stores all information obtained while crawling, such as the URL and the local path for each of the images, as well as any errors that may have been encoutered for any of the websites.

