import scrapy
import csv
import re
import urllib
import os
import traceback
import json
from urllib.parse import urljoin
import requests

class QuotesSpider(scrapy.Spider):
    name = "logo_scraper"

    def start_requests(self):
        headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"}
        url_list = []

        #Create empty CSV and JSON files to add scraped data
        rowtitles = ['Website','favicon_path','img_logo_path','div_logo_path','header_logo_path','header_img_path','og_path','twtr_path','bootstrap_path','svg_resp','css_logo_path','ERROR']
        with open('data.csv','w', newline='') as fd:
            spamwriter = csv.writer(fd)
            spamwriter.writerow(rowtitles)
        with open('data.json', 'w') as outfile:
            json.dump({},outfile)
        with open('websites.csv') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',')
            for row in spamreader:
                url_list.append('http://www.'+row[0])
        for url in url_list:
            yield scrapy.Request(url=url, callback=self.parse, headers=headers,meta={'download_timeout': 40, 'source_url': url})


    #Process the data scraped to download and save the image in its respective folder.
    def process_image(self,url,page,category,response = None,attrib = None, link = None, svg = False):
        #save img,return True/False[path,downloaded], return path
        headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"}
        ext_list = ('.apng','.bmp','.gif','.ico','.cur','.jpg','.jpeg','.jfif','.pjpeg','.pjp','.png','.svg','.tif','.tiff','.webp')

        FOUND = False
        local_path = "./logos/"+page+"/"+category
        path = None
        joined_path = None
        if response:
            for resp in response:
                if attrib:
                    if attrib in resp.attrib.keys():
                        path = resp.attrib[attrib]
                        if len(path) > 2:
                            FOUND = True
                    if not FOUND:
                        if 'data-src' in resp.attrib.keys():
                            path = resp.attrib['data-src']
                            FOUND = True
                        #find attribute any value with extension of image file
                        else:
                            attr_vals = resp.attrib
                            for val in attr_vals:
                                if resp.attrib[val].endswith(ext_list):
                                    path = resp.attrib[val]
                                    FOUND = True
                                if FOUND:
                                    break
                if FOUND:
                    break

        if link:
            path = link
            FOUND = True

        if path:
            img_data = None
            try:
                joined_path = urljoin(url,path)
                img_data = requests.get(joined_path,headers=headers)
            except:
                print(joined_path)
                img_data = None

        if FOUND and img_data:
            path = path.strip('/')
            ext = path.split('.')[-1].split('?')[0].strip('.')
            if not os.path.isdir(local_path):
                os.makedirs(local_path)
            if len(ext) > 4:
                ext = 'png'
            local_path = local_path.strip('/')
            local_path += '/img.'+ext 
            local_path = local_path.strip('/')
            open(local_path, 'wb').write(img_data.content)
        else:
            FOUND = False
        
        if svg:
            path = None
            size = -1
            svg_data = ''
            for i in response:
                data = i.get()
                if len(data) > size:
                    svg_data = data
                    size = len(data)
            
            if not os.path.isdir(local_path):
                os.makedirs(local_path)
            local_path += "/img.svg" 
            f = open(local_path, "w")
            if 'xmlns' in svg_data:
                f.write(svg_data)
            else:
                svg_data = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg">'+svg_data+'</svg>'
                f.write(svg_data)
            f.close()
            FOUND = True
        if FOUND:
            return {'FOUND':FOUND,'data':{"url":path,"file":local_path}}
        else:
            return {'FOUND':FOUND}


    def parse(self, response):
        ext_list = ('.apng','.bmp','.gif','.ico','.cur','.jpg','.jpeg','.jfif','.pjpeg','.pjp','.png','.svg','.tif','.tiff','.webp')
        headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"}
        page = response.meta['source_url'].split('.')[1]
        print(page)
        url = response.url
        print(url)
        if not os.path.isdir("./logos/"+page):
            os.makedirs("./logos/"+page)
        favicon_resp = {'FOUND':False}
        img_logo_resp = {'FOUND':False}
        div_logo_resp = {'FOUND':False}
        header_logo_resp = {'FOUND':False}
        header_img_resp = {'FOUND':False}
        og_resp = {'FOUND':False}
        twtr_resp = {'FOUND':False}
        bootstrap_resp = {'FOUND':False}
        svg_resp = {'FOUND':False}
        css_logo_resp = {'FOUND':False}
        ERROR = False
        
        try:
            #1. Favicon
            favicon = response.xpath('//link[contains(@rel, "icon")]')
            if favicon:
                favicon_resp = self.process_image(url,page,'favicon',favicon,'href')
            else:
                favicon = urljoin(url,'favicon.ico')
                favicon_resp = self.process_image(url,page,'favicon',None,None,favicon)

            #2. Image with logo in its name
            img_logo = response.xpath('//img[contains(@src, "logo")]')
            if img_logo:
                img_logo_resp = self.process_image(url,page,'img_logo',img_logo,'src')
            else:
                img_logo_resp = {'FOUND':False}

            #3. div with logo in its attribute
            div_logo = response.xpath('//div[contains(@*, "logo")]')
            if div_logo:
                div_logo_resp = self.process_image(url,page,'div_logo',div_logo[0].xpath('.//img'),'src')
            else:
                div_logo_resp = {'FOUND':False}

            #4. header with logo in its attribute
            header_logo = response.xpath('//header[contains(@*, "logo")]')
            if header_logo:
                header_logo_resp = self.process_image(url,page,'header_logo',header_logo[0],'src')
            else:
                header_logo_resp = {'FOUND':False}

            #5. Header with an image
            header_img = response.xpath('//header')
            if header_img:
                header_img_resp = self.process_image(url,page,'header_img',header_img[0].xpath('.//img'),'src')
            else:
                header_img_resp = {'FOUND':False}

            #6. Opengraph
            og_img = response.xpath('//meta[@property="og:image"]')
            if og_img:
                og_resp = self.process_image(url,page,'og_img',og_img,'content')
            else:
                og_resp = {'FOUND':False}

            #7. Twittergraph
            twtr = response.xpath('//meta[contains(@name,"twitter:image")]')
            if twtr:
                twtr_resp = self.process_image(url,page,'twtr',twtr,'content')
            else:
                twtr_resp = {'FOUND':False}

            #8. Navbar with an image - [bootstrap]
            bootstrap = response.xpath('//div[contains(@*, "navbar")]')
            if bootstrap:
                bootstrap_resp = self.process_image(url,page,'bootstrap',bootstrap[0].xpath('.//img'),'src')
            else:
                bootstrap = response.xpath('//header[contains(@*, "navbar")]')
                if bootstrap:
                    bootstrap_resp = self.process_image(url,page,'bootstrap',bootstrap[0].xpath('.//img'),'src')
                else:
                    bootstrap_resp = {'FOUND':False}

            #9. SVG logos:
            svg = response.xpath('//svg')
            if svg:
                svg_resp = self.process_image(url,page,'svg',svg,None,None,True)
            else:
                svg_resp = {'FOUND':False}

            #10. CSS - For sites that embed the logo in CSS
            class_names = []
            for i in response.xpath('//a[contains(@*, "logo")]'):
                if 'class' in i.attrib.keys():
                    class_names.append(i.attrib['class'])
            css_path = response.xpath('//link[@rel="stylesheet"][contains(@href,"style.css")]')
            
            if css_path:
                css_path = css_path[0].attrib['href']
                css_path = urljoin(url, css_path)
                css_data = requests.get(css_path).text
                css_logo_path = None
                for class_name in class_names:
                    path = re.findall('\.'+class_name+'{.*?background-image:url\((.*?)\);', css_data)
                    if path:
                        path = path[0]
                        css_logo_path = urljoin(css_path, path)
                        break
                if css_logo_path:
                    css_logo_resp = self.process_image(url,page,'css_logo',None,None,css_logo_path)
                
            else:
                css_logo_resp = {'FOUND':False}
        except: 
            ERROR = traceback.format_exc().replace('\n','')
            print("ERROR PAGE:",page)
            print("ERROR PAGE:",ERROR)

        #Save the collected info in a dict/2d Array
        page_data = {}
        json_data = {}
        page_data['favicon'] = favicon_resp
        page_data['img_logo'] = img_logo_resp
        page_data['div_logo'] = div_logo_resp
        page_data['header_logo'] = header_logo_resp
        page_data['header_img'] = header_img_resp
        page_data['og'] = og_resp
        page_data['twtr'] = twtr_resp
        page_data['bootstrap'] = bootstrap_resp
        page_data['svg_logo'] = svg_resp
        page_data['css_logo'] = css_logo_resp
        
        rowvalues = [page,favicon_resp,img_logo_resp,div_logo_resp,header_logo_resp,header_img_resp,og_resp,twtr_resp,bootstrap_resp,svg_resp,css_logo_resp,ERROR]
        json_data[page] = {'data':page_data,'error':ERROR}

        with open('data.csv','a', newline='') as fd:
            spamwriter = csv.writer(fd)
            spamwriter.writerow(rowvalues)
        old_data = ''

        with open('data.json', 'r') as outfile:
            old_data = json.load(outfile)

        old_data.update(json_data)

        with open('data.json', 'w') as outfile:
            json.dump(old_data,outfile)